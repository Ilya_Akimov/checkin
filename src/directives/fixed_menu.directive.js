(function() {
  'use strict';

  angular
    .module('triblans')
    .directive('fixedMenu', FixedMenu);

  function FixedMenu() {
    return {
      restrict : "A",
      scope: {},
      link: function(){
        var menu = $('.menu_section .navbar-default');
        var top_height_for_margin = $('.main_header').height() + $('.content .caption').height() + 30;
        function scroll() {
          if ($(window).scrollTop() >= top_height_for_margin) {
            menu.addClass('navbar-fixed-top');
            $('body').addClass('fixed-menu');
          } else {
            menu.removeClass('navbar-fixed-top');
            $('body').removeClass('fixed-menu');
          }
        }
        document.onscroll = scroll;
      }
    };
  }

})();