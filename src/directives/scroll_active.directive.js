(function() {
  'use strict';

  angular
    .module('triblans')
    .directive('scrollActive', ScrollActive);

  function ScrollActive() {
    return {
      restrict : "A",
      scope: {
        markers: "="
      },
      link: function(scope) {

        $(window).scroll(function(e) {
          var notes = $('.note');
          var scroll_top = $(window).scrollTop() + 250;


          e.stopPropagation();
          e.preventDefault();



          notes.each(function() {
            if ( scroll_top >= $(this).offset().top
              &&
              scroll_top <= ($(this).offset().top + $(this).height())
            ) {
              var id = $(this).attr('id').replace('note','');
              var $sliderId = $('#slide_pager' + id);

              if (! $sliderId.hasClass('slick-current') ) {
                $sliderId.click();


                for (var i=0; i < scope.markers.length; i++) {
                  if (scope.markers[i].idKey != id) {
                    scope.markers[i].icon = {
                      url: 'images/icon_red_circle.png',
                      scaledSize: { width: 20, height: 20 },
                      anchor: { x: 10, y: 10 },
                      status: ''
                    };
                  } else {
                    scope.markers[i].icon = {
                      url: scope.markers[i].image,
                      scaledSize: { width: 30, height: 30 },
                      anchor: { x: 15, y: 15 },
                      status: 'active'
                    };
                  }
                }

                scope.$apply();

              }

            }
          });
        });

      }
    };
  }

})();