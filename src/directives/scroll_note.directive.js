(function() {
  'use strict';

  angular
    .module('triblans')
    .directive('scrollNote', ScrollNote);

  function ScrollNote() {
    return {
      restrict : "A",
      scope: {
        scrollNote: '@',
        center: '='
      },
      link: function(scope, element) {
        $(element).on('click selectcity', function(e) {
          var acriveLoader = $('.loader-page');
          var $noteId = $('#note'+scope.scrollNote);

          if ($noteId.attr('id')) {
            acriveLoader.css("display", "block");

            e.stopPropagation();
            e.preventDefault();
            $('html, body').animate({
              scrollTop: $noteId.offset().top - 200
            }, {
              complete: function () {
                acriveLoader.css("display","none");
              },
              duration: 1000
            });
              if (scope.center) {
                var placeCenter = $(this).attr('data-place-center');
                scope.$apply(function () {
                  scope.center = JSON.parse(placeCenter);
                });
              }
          }
        });

      }
    };
  }

})();