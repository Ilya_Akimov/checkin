(function() {
  'use strict';

  angular
    .module('triblans')
    .directive('checkinScrollActive', checkinScrollActive);

  checkinScrollActive.$inject = ['utils'];

  function checkinScrollActive(utils) {
    return {
      restrict : "A",
      scope: {
        checkinScrollActive: '=',
        center: "="
      },
      link: function(scope) {
        /**
         * End of user scroll process.
         */
        var $window = $(window);
        var onScroll = utils.debounce(scrollEnd, 300, false);

        $window.on('scroll', onScroll);

        function scrollEnd() {
          var notes = $('.city_content');
          var scroll_top = $window.scrollTop() + 250;

          notes.each(function () {
            var $this = $(this);
            var scrollHigherTop = scroll_top >= $this.offset().top;
            var lowerBottom = scroll_top <= $this.offset().top + $this.innerHeight();

            if (scrollHigherTop && lowerBottom) {
              var id = $this.attr('id').replace('Content', '');
              var placeCenter = $this.attr('data-place-center');
              var $id = $('#' + id);

              if (!$id.hasClass('slick-current')) {
                $('.slick-slider').slick('slickGoTo', parseInt($id.attr('data-place-id')));
                if (!(scope.center == undefined)) {
                  scope.center = JSON.parse(placeCenter);
                  scope.$digest();
                }

                return false;
              }
            }
          });
        }
      }
    };
  }

})();