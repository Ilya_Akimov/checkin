(function() {
  'use strict';

  angular
    .module('triblans')
    .directive('likeActive', LikeActive);

  function LikeActive() {
    return {
      restrict : "E",
      scope: {
        like : '='
      },
      template: '<span>{{like}}</span>',
      link: function(scope, element){
        element.bind('click', function() {
          if (element.hasClass('active')) {
            element.removeClass('active');
            scope.like = scope.like - 1;
          }
          else {
            element.addClass('active');
            scope.like = scope.like + 1;
          }
          scope.$apply();
        })
      }
    };
  }

})();