(function() {
  'use strict';

  angular
    .module('triblans')
    .directive('tripCheckinSlider', tripCheckinSlider);

  function tripCheckinSlider() {
    return {
      restrict : "A",
      scope: false,
      link: function(scope, element) {
        $(function() {
          var $element = $(element);
          var slidesToshow;


          if ($(window).width() <= 1024 && $(window).width() > 768) {
            slidesToshow = 5;
          }
          else if ($(window).width() <= 768 && $(window).width() > 436) {
            slidesToshow = 3;
          }
          else if ($(window).width() <= 436 && $(window).width() > 375 ) {
            slidesToshow = 2;
          }
          else  if ($(window).width() <= 375) {
            slidesToshow = 1
          }
          else {
            slidesToshow = 7;
          }

          $element.slick({
            arrows: true,
            centerMode: true,
            draggable: false,
            focusOnSelect: true,
            nextArrow: $('.arrow-right', $element),
            prevArrow: $('.arrow-left', $element),
            slidesToShow: slidesToshow,
            slidesToScroll: 1,
            slide: '.slide'
          });
          $(".scroll-note").click(function() {
              angular.element('.slick-center').trigger('click');
          });
        });
      }
    };
  }

})();