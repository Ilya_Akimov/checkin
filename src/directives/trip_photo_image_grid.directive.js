(function() {
  'use strict';

  angular
    .module('triblans')
    .directive('imageGrid', ImageGrid);

  ImageGrid.$inject = ['$compile', '$rootScope', '$templateCache'];

  function ImageGrid($compile, $rootScope, $templateCache) {
    return {
      restrict : "E",
      scope: {
        coords: '=',
        countryId: '@',
        cityId: '@',
        images: '='
      },
      link: function(scope, element) {
        activateGrid();
        addHover();

        /**
         * Init imageGrid plugin.
         */
        function activateGrid() {
          element.imagesGrid({
            images: scope.images,
            cells: 6,
            getViewAllText: function() {
              return '';
            }
          });
        }

        /**
         * Add hover elements to imageGrid items.
         */
        function addHover() {
          var newScope;
          var hoverElem;
          var $items = $('.image-wrap', element);

          $items.each(function(index) {
            var $item = $(this);

            newScope = $rootScope.$new(true);
            newScope.checkin = scope.images[index];
            newScope.openModal = openModal;
            hoverElem = $compile($templateCache.get('tripPhotoImageGrid.html'))(newScope);
            $item.append(hoverElem);
          });
        }

        /**
         * Run modal handler and set map coords.
         * @param {Object} $event
         * @param {string} modalId
         * @param {Object} center
         */
        function openModal($event, modalId, center) {
          $event.stopPropagation();

          scope.coords = center;

          $(modalId).modal();
        }
        $('.image-wrap').on('click', function () {
          $('html').css('overflow', 'hidden');
          setTimeout(function () {
            $('.modal-close').on('click', function () {
              $('html').css('overflow', 'auto');
            });
          }, 200);
        });
      }
    };
  }

})();