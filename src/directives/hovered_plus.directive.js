(function() {
    'use strict';

    angular
      .module('triblans')
      .directive('hoveredPlus', HoveredPlus);

    function HoveredPlus() {
        return {
            restrict : "A",
            scope: {
                hoveredPlus: "="
            },
            link: function(scope, element){

                $(element).mouseover(function() {
                    $('#plus'+scope.hoveredPlus).addClass('hovered_add_button');
                });

                $(element).mouseout(function() {
                    $('#plus'+scope.hoveredPlus).removeClass('hovered_add_button');
                });
            }
        };
    }

})();