(function() {
  'use strict';

  angular
    .module('triblans')
    .directive('imageGrid', ImageGrid);

  function ImageGrid() {
    return {
      restrict : "E",
      scope: {
        images: '='
      },
      link: function(scope, element){
        element.imagesGrid({
          images: scope.images,
          getViewAllText: function(imagesCount) {
            return imagesCount - 5 + '+';
          }
        });
      }
    };
  }

})();