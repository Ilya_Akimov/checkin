(function() {
  'use strict';

  angular
    .module('triblans')
    .directive('hideEndComment', hideEndComment);

  hideEndComment.$inject =['$timeout', '$compile'];

  function hideEndComment($timeout, $compile) {
    return {
      restrict : "A",
      scope: {},

      link: function(scope, element) {
        $timeout(function () {
          if (element[0].offsetHeight >100) {
            element.addClass('shown-comment');
            var template = $compile('<a href="#"> see more <a>')(scope);
            element.parent().append(template);
          }
        }, false);
      }
    };
  }

})();