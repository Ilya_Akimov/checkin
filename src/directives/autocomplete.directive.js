(function() {
  'use strict';

  angular
    .module('triblans')
    .directive('gmapAutocomplete', gmapAutocomplete);


  function gmapAutocomplete() {
    return {
      restrict : "A",
      scope: {
        gmapAutocomplete: "=",
        coords: '=',
        modalData: '='
      },
      link: function(scope,element){
        var autocomplete = new google.maps.places.Autocomplete(element.get(0));
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
          var geoComponents = autocomplete.getPlace();
          var latitude = geoComponents.geometry.location.lat();
          var longitude = geoComponents.geometry.location.lng();
          var location = geoComponents.formatted_address;
          var reviews = geoComponents.reviews;
          $('.click_modal').trigger('click');
          scope.$apply(function() {
            scope.coords = {
              latitude: latitude,
              longitude: longitude
            };
            scope.modalData = {
              rating: 0,
              place_name: location
            }
          });
        });

      }
    }
  }
})();