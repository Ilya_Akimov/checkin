(function() {
  'use strict';

  angular
    .module('triblans')
    .directive('sliderDirective', SliderDirective);

  function SliderDirective() {
    return {
      restrict : "A",
      scope: { show: '=' },
      link: function(scope,element){
        $( document ).ready(function() {
          $(element).slick({
            slidesToShow: scope.show,
            slidesToScroll: 1,
            centerMode: false,
            focusOnSelect: true,
            arrows: false
          });

          $(".scroll-note.arrow-left").click(function() {
            var id = $('.slick-current').attr('id').replace('slide_pager','')-1;
            if ( id < 1 ) {
              id = scope.show
            }
            $('#date'+id).click();
          });
          $(".scroll-note.arrow-right").click(function() {

            var id = $('.slick-current').attr('id').replace('slide_pager','');
            id++;
            if ( id > scope.show ) {
              id = 1
            }
            $('#date'+id).click();
          });
        });
      }
    };
  }

})();