(function() {
  'use strict';

  angular
    .module('triblans')
    .directive('shownAria', shownAria);

  function shownAria() {
    return {
      restrict : "A",
      scope: {
        shownAria: '=',
        dropdownPlaces: '='
      },
      link: function(scope, element) {
        var $slider = $('.slider', element);
        var $slides;
        $slider.on('init', initSlider);
        $slider.on('afterChange', sliderUpdate);

        //////////////////////////////////////////////////////////////
        // COMMON
        //////////////////////////////////////////////////////////////

        /**
         * Handler of slider initialization.
         */
        function initSlider() {
          $slides = $('.slick-slide', $slider);
          sliderUpdate();
        }

        /**
         * Handler of slider update event.
         */
        function sliderUpdate() {
          scope.$apply(function() {

            setDropdownVisibilityAll(false);
            $slides.filter('[aria-hidden="false"]').each(handleActivePlace);
            setDropdownVisibilityElement();
          });
          scope.$apply(function() {
            showCountry();
          });
          controlCountryVisibility();
        }

        /**
         * Get modal place.
         * @param {number} placeId
         * @returns {Object|null}
         */
        function getPlace(placeId) {
          var model = null;

          placeId *= 1;

          scope.shownAria.some(function(place) {
            if (place.id === placeId) {
              model = place;

              return false;
            }
          });

          return model;
        }

        //////////////////////////////////////////////////////////////
        // DROPDOWN VISIBILITY
        //////////////////////////////////////////////////////////////

        /**
         * Set sliderShow option in all place models.
         * @param {boolean} isVisible
         */
        function setDropdownVisibilityAll(isVisible) {
          angular.forEach(scope.shownAria, function (place) {
            place.sliderShow = isVisible;
          });
        }
        /**
         * Set sliderShow option in the place models.
         * @param {number} placeId
         * @param {boolean} isVisible
         */
        function setDropdownVisibility(placeId, isVisible) {
          var place = getPlace(placeId);

          place.sliderShow = isVisible;
        }

        /**
         * Set visibility in dropdown item.
         * @param {number} index
         * @param {HTMLElement} elem
         */
        function handleActivePlace(index, elem) {
          var $elem = $(elem);
          var placeId = $elem.attr('data-place-id');

          setDropdownVisibility(placeId, true);
        }

        /**
         * Create massive elements from drop down sort by position on slider.
         */
        function setDropdownVisibilityElement() {
          var breakpoint;

          scope.shownAria.some(function (place) {
            if (place.sliderShow === true) {
              breakpoint = place.id;
              return true;
            }
          });
          var count = 0;
          var i;
          for (i = breakpoint; i<scope.shownAria.length; i++) {
            if (scope.shownAria[i].sliderShow === false){
              scope.dropdownPlaces[count] = scope.shownAria[i];
              count++;
            }
          }
          for (i = 0; i < breakpoint; i++) {
            if (scope.shownAria[i].sliderShow === false){
              scope.dropdownPlaces[count] = scope.shownAria[i];
              count++;
            }
          }
        }

        //////////////////////////////////////////////////////////////
        // SHOW COUNTRY
        //////////////////////////////////////////////////////////////

        var $showCountries = $('.show-country', element);
        var $showCountryItems;

        /**
         * Show countries in .show-country for .show-country-item elements.
         */
        function showCountry() {
          hideAllCountries();

          $showCountries.each(function() {
            var $showCountry = $(this);
            $showCountryItems = $('.show-country-item', $showCountry);

            var $active = $showCountryItems.filter('.active, .slick-active');
            var currentItem;
            var leftItem;
            var currentModel;
            var leftModel;
            var currentPlaceId;

            for (var i = $active.length - 1; i > 0; i--) {
              currentItem = $active.eq(i);
              leftItem = $active.eq(i-1);

              currentPlaceId = currentItem.attr('data-place-id');
              currentModel = getPlace(currentPlaceId);
              leftModel = getPlace(leftItem.attr('data-place-id'));

              if (currentModel.country !== leftModel.country) {
                showItemCountry(currentPlaceId);
              }
            }

            showItemCountry($active.first().attr('data-place-id'));
          });
        }

        /**
         * Set countryShow option in the place model.
         * @param {number} placeId
         */
        function showItemCountry(placeId) {
          var place = getPlace(placeId);

          place.countryShow = true;
        }

        /**
         * Set countryShow option equal to false in all place models.
         */
        function hideAllCountries() {
          angular.forEach(scope.shownAria, function(place) {
            place.countryShow = false;
          });
        }

        /**
         * Set visibility to countries depend on the model.
         */
        function controlCountryVisibility() {
          $slides.each(function(index, elem) {
            var $slide = $(elem);
            var placeId = $slide.attr('data-place-id');
            var model = getPlace(placeId);

            if (model.countryShow) {
              $slide.addClass('country-is-shown');
            } else {
              $slide.removeClass('country-is-shown');
            }
          });
        }
      }
    }
  }

})();