(function() {
  'use strict';

  angular
    .module('triblans')
    .controller('MainController', MainController);

  MainController.$inject = [ 'MapService', 'MainDataService', '$scope' ];
  function MainController( MapService, MainDataService, $scope ) {

    var vm = this;
    vm.coords = {
      latitude: 0,
      longitude: 0
    };
    vm.modalData = {
      rating: 0,
      place_name: ""
    };

    vm.trip = MainDataService.getTrip();

    // Needed for open and close add-chekin-input
    vm.add_check_in = -1;
    vm.addCheckIn = function(id){
      if (vm.add_check_in == id) {
        vm.add_check_in = -1;
      } else {
        vm.add_check_in = id;
      }
    };
    vm.closeAdd = function(id){
      if (vm.add_check_in == id) {
        vm.add_check_in = -1;
      }
    };

    // Counting total of days
    vm.trip.total_days = vm.trip.days.length;

    // For hide and show map
    vm.mapHide = false;

    // Function for using with ng-repeat to create few elements. For example Start-Rating
    vm.range = function(n) {
      return new Array(n);
    };

    vm.isExist = function(day) {
      if (Object.keys(day.check_in).length != 0) {
        return 'check_in';
      } else if (day.photos.length!=0) {
        return 'photos';
      } else {
        return 'text';
      }
    };

    // Getting map with all poligons and markets
    vm.google_map = MapService.createTripMap(vm.trip.days);

    // Check_IN modal with correct map
    vm.modalMap = MapService.getModalMap();
    $('#checkin-modal').on('show.bs.modal', function() {
      setTimeout(function () {
        vm.control = {};
        vm.modalMap.control.refresh();
      }, 1000)
    });

    // Function that used like flag for open and close comment textarea
    vm.write_a_comment = false;
    vm.SaveComment = function(keyEvent, id, comment) {
      if (keyEvent.which === 13){
        angular.forEach(vm.trip.days, function(day) {
          if ( day.id == id ) {
            var new_comment = {
              time: "15 minutes ago",
              commentator_first_name: 'Annis',
              commentator_second_name: 'Horn',
              commentator_photo: "images/avatar.png",
              text: comment
            };
            day.comments.push(new_comment);
            vm.new_comment[id] = '';
          }
        })
      }
    };

    vm.commentFlag = -1;
    vm.showComments = function(id) {
      vm.commentFlag = id;
    };



// Datepicker temp

    vm.today = function() {
      vm.dt = new Date();
    };
    vm.today();

    vm.clear = function() {
      vm.dt = null;
    };

    vm.inlineOptions = {
      customClass: getDayClass,
      minDate: new Date(),
      showWeeks: true
    };

    vm.dateOptions = {
      // dateDisabled: disabled,
      formatYear: 'yy',
      maxDate: new Date(2020, 5, 22),
      minDate: new Date(),
      startingDay: 1
    };

    vm.Range = function(start, end) {
      var result = [];
      for (var i = start; i <= end; i++) {
        result.push(i);
      }
      return result;
    };

    vm.toggleMin = function() {
      vm.inlineOptions.minDate = vm.inlineOptions.minDate ? null : new Date();
      vm.dateOptions.minDate = vm.inlineOptions.minDate;
    };

    vm.toggleMin();

    vm.open1 = function() {
      vm.popup1.opened = true;
    };

    vm.open2 = function() {
      vm.popup2.opened = true;
    };

    vm.setDate = function(year, month, day) {
      vm.dt = new Date(year, month, day);
    };

    vm.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    vm.format = vm.formats[0];
    vm.altInputFormats = ['M!/d!/yyyy'];

    vm.popup1 = {
      opened: false
    };

    vm.popup2 = {
      opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    vm.events = [
      {
        date: tomorrow,
        status: 'full'
      },
      {
        date: afterTomorrow,
        status: 'partially'
      }
    ];

    vm.timepicker = new Date();
    vm.timePickerOptions = {
      step: 15,
      timeFormat: 'g:ia'
      // appendTo: 'body'
    };

    function getDayClass(data) {
      var date = data.date,
        mode = data.mode;
      if (mode === 'day') {
        var dayToCheck = new Date(date).setHours(0,0,0,0);

        for (var i = 0; i < vm.events.length; i++) {
          var currentDay = new Date(vm.events[i].date).setHours(0,0,0,0);

          if (dayToCheck === currentDay) {
            return vm.events[i].status;
          }
        }
      }

      return '';
    }

    /**
     * Watch to date on modal and convert to timestamp.
     */
      $scope.$watch(function () {
          return vm.dt;
        }, function(newValue) {
          vm.dtstamp = Date.parse(newValue);
          console.log(newValue, vm.dtstamp);
        }
      );
  }

})();