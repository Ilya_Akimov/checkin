(function() {
    'use strict';
    
    angular
        .module('triblans')
        .controller('TripPhotoController', TripPhotoController);

    TripPhotoController.$inject = ['TripPhotoDataService', 'PhotoMapService'];
    function TripPhotoController( TripPhotoDataService, PhotoMapService ) {

      var vm = this;

      vm.center = {};
      vm.dropdown_places = [{}];

      vm.trip = TripPhotoDataService.getTrip();

      // Function for using with ng-repeat to create few elements. For example Start-Rating
      vm.range = function(n) {
          return new Array(n);
      };

      // Getting map with all poligons and markets
      // vm.google_map = MapService.createTripMap(vm.trip.days);


      vm.Range = function(start, end) {
          var result = [];
          for (var i = start; i <= end; i++) {
              result.push(i);
          }
          return result;
      };

      //Convert data trip for navigation panel.
      vm.short_trip = TripPhotoDataService.getShortTrip();
      var count = 0;
      vm.places = [];

      for (var i = 0; i < vm.short_trip.length; i++) {
        for (var j = 0; j < vm.short_trip[i].city.length; j++, count++) {
          vm.places[count] = {
            id: count,
            country: vm.short_trip[i].country,
            city: vm.short_trip[i].city[j].name,
            sliderShow: true,
            countryShow: true
          }
        }
      }

      vm.coords = {
        latitude: 0,
        longitude: 0
      };

      // Check_IN modal with correct map
      vm.modalMap = PhotoMapService.getModalMap();
      $('.check-in-modal').on('show.bs.modal', function() {
        setTimeout(function () {
          vm.control = {};
          vm.modalMap.control.refresh();
        }, 1000);
      });
    }

})();