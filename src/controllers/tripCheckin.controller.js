(function() {
    'use strict';
    
    angular
        .module('triblans')
        .controller('TripCheckinController', TripCheckinController);
        
    TripCheckinController.$inject = [ 'TripCheckinMapService', 'TripCheckinDataService'];
    function TripCheckinController( TripCheckinMapService, TripCheckinDataService) {

      var vm = this;

      vm.trip = TripCheckinDataService.getTrip();
      vm.dropdown_places = [{}];

      // Function for using with ng-repeat to create few elements. For example Start-Rating
      vm.range = function(n) {
          return new Array(n);
      };

      // Getting map with all poligons and markets
      vm.google_map = TripCheckinMapService.createTripMap(vm.trip.country);


      vm.Range = function(start, end) {
          var result = [];
          for (var i = start; i <= end; i++) {
              result.push(i);
          }
          return result;
      };

      // Convert data trip for navigation panel.
      vm.short_trip = TripCheckinDataService.getShortTrip();
      var count = 0;
      vm.places = [];

      for (var i = 0; i < vm.short_trip.length; i++) {
        for (var j = 0; j < vm.short_trip[i].city.length; j++, count++) {
          vm.places[count] = {
            id: count,
            country: vm.short_trip[i].country,
            city: vm.short_trip[i].city[j].name,
            sliderShow: true,
            countryShow: true,
            center: vm.short_trip[i].city[j].center
          }
        }
      }
    }

})();