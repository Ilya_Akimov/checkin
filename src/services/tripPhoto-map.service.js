(function() {
  'use strict';

  angular
    .module('triblans')
    .service('PhotoMapService', PhotoMapService);

  PhotoMapService.$inject = [];
  function PhotoMapService () {

    // for modal map
    this.getModalMap = function () {
      var map_center = {
        latitude: 0,
        longitude: 0
      };
      var map;

      map = {
        center: map_center,
        zoom: 16,
        bounds: {},
        options: {
          scrollwheel: false,
          disableDefaultUI: true
          // mapTypeId: google.maps.MapTypeId.map_center
        },
        control: {}
      };
      return map;
    };


  }


})();