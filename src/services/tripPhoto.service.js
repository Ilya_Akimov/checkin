(function() {
  'use strict';

  angular
    .module('triblans')
    .service('TripPhotoDataService', TripPhotoDataService);

  function TripPhotoDataService() {

    var short_trip = [
      {
        id: 1,
        country: 'Italy',
        city:
          [
            {
              id: 1,
              name:'Venice'
            },
            {
              id: 2,
              name:'Rome'
            },
            {
              id: 3,
              name: 'Florencia'
            }
          ]
      },
      {
        id: 2,
        country: 'United Kingdom',
        city:
          [
            {
              id: 1,
              name: 'London'
            }
          ]
      },
      {
        id: 3,
        country: 'Norway',
        city:
          [
            {
              id: 1,
              name: 'Geneva'
            }
          ]
      },
      {
        id: 4,
        country: 'Brazil',
        city:
          [
            {
              id: 1,
              name: 'Brasilia'
            }
          ]
      },
      {
        id: 5,
        country: 'Canada',
        city:
          [
            {
              id: 1,
              name: 'Ottawa'
            }
          ]
      },
      {
        id: 6,
        country: 'Cuba',
        city:
          [
            {
              id: 1,
              name: 'Havana'
            }
          ]
      },
      {
        id: 7,
        country: 'Morocco',
        city:
          [
            {
              id: 1,
              name: 'Rabat'
            },
            {
              id: 2,
              name: 'Fes'
            },
            {
              id: 3,
              name: 'Casabla'
            }
          ]
      },
      {
        id: 8,
        country: 'Finland',
        city:
          [
            {
              id: 1,
              name: 'Helsinki'
            }
          ]
      },
      {
        id: 9,
        country: 'Germany',
        city:
          [
            {
              id: 1,
              name: 'Berlin'
            }
          ]
      }
    ];

    var trip = {
      id: 1,
      title: "The Theasures of France",
      traveler_first_name: "Julia",
      traveler_second_name: "McBride",
      traveler_photo: "images/avatar.png",
      date: "Oct 24, 2016",
      check_ins: 67,
      photos: 92,
      country: [
        {
          id: 1,
          flag: "images/Italy.png",
          name: 'Italy',
          city: [
            {
              id:1,
              name: 'Venice',
              photos: [
                {
                  src: 'images/c9dad998-8644-4ff5-8a9b-a2b710c49f8e.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 45.43540473801,
                    longitude: 12.34181051852
                  }
                },
                {
                  src: 'images/trip_photo2.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 45.43540473801,
                    longitude: -12.34181051852
                  }
                },
                {
                  src: 'images/trip_photo3.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: -45.43540473801,
                    longitude: 12.34181051852
                  }
                },
                {
                  src: 'images/trip_photo3.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 10.43540473801,
                    longitude: 12.34181051852
                  }
                },
                {
                  src: 'images/trip_photo4.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 34.43540473801,
                    longitude: 12.34181051852
                  }
                },
                {
                  src: 'images/trip_photo5.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 0.43540473801,
                    longitude: 12.34181051852
                  }
                }
              ]
            },
            {
              id:2,
              name: 'Rome',
              photos: [
                {
                  src: 'images/trip_photo1.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 41.90214252055,
                    longitude: 12.495993256591
                  }
                },
                {
                  src: 'images/trip_photo2.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 41.90214252055,
                    longitude: 12.495993256591
                  }
                },
                {
                  src: 'images/trip_photo3.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 41.90214252055,
                    longitude: 12.495993256591
                  }
                },
                {
                  src: 'images/trip_photo3.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 41.90214252055,
                    longitude: 12.495993256591
                  }
                },
                {
                  src: 'images/trip_photo4.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 41.90214252055,
                    longitude: 12.495993256591
                  }
                },
                {
                  src: 'images/trip_photo5.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 41.90214252055,
                    longitude: 12.495993256591
                  }
                }
              ]
            },
            {
              id:3,
              name: 'Florencia',
              photos: [
                {
                  src: 'images/trip_photo1.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 43.78190855352,
                    longitude: 11.23965799999
                  }
                },
                {
                  src: 'images/trip_photo2.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 43.78190855352,
                    longitude: 11.23965799999
                  }
                },
                {
                  src: 'images/trip_photo3.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 43.78190855352,
                    longitude: 11.23965799999
                  }
                },
                {
                  src: 'images/trip_photo3.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 43.78190855352,
                    longitude: 11.23965799999
                  }
                },
                {
                  src: 'images/trip_photo4.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 43.78190855352,
                    longitude: 11.23965799999
                  }
                },
                {
                  src: 'images/trip_photo5.jpg',
                  title: 'My trip across France',
                  address: 'Louvre Museum, Paris, France',
                  center: {
                    latitude: 43.78190855352,
                    longitude: 11.23965799999
                  }
                }
              ]
            }
           ]
        }
      ]
    };

    this.getTrip = function() {
      return trip;
    };
    this.getShortTrip =function() {
      return short_trip;
    }
  }

})();