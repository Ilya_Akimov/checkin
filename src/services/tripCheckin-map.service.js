(function() {
  'use strict';

  angular
    .module('triblans')
    .service('TripCheckinMapService', TripCheckinMapService);

  TripCheckinMapService.$inject = [];
  function TripCheckinMapService () {

    // creating paths for images on map markers
    function getPaths (all_trip) {
      var paths = [];

      angular.forEach(all_trip, function(value1) {
        angular.forEach(value1.city, function (value2) {
          angular.forEach(value2.content, function (value) {
            var path = {};
            path.image = value.icon;
            path.latitude = value.latitude;
            path.longitude = value.longitude;
            path.city = value2.name;
            path.id = value.id;
            paths.push(path);
          })
        })
      });

      return paths;
    }

    // create map with center values
    function getMap () {
        
      var map_center = {
        latitude:45.43697585375198,
        longitude:12.3410595
      };
      var map;

      map = {
        idKey: 1,
        center: map_center,
        zoom: 13,
        options: {
          scrollwheel: true,
          mapTypeId: google.maps.MapTypeId.TERRAIN,
          disableDefaultUI: true
        }
      };
      return map;
    }

    function getMarkers (all_trip) {

      var paths = getPaths(all_trip);
      var markers = [];

      angular.forEach(paths, function(value, key) {
        var marker = {
          idKey: key,
          coords: {
            latitude: value.latitude,
            longitude: value.longitude
          },
          icon: value.image,
          events: {
            click: function () {

              $('html, body').animate({
                scrollTop: $("#" + value.city + value.id).offset().top - 200
              }, 1000);
            }
          }
        };
        markers.push(marker);
      });

      return markers;
    }

    // main function which return completed map to controller
    this.createTripMap = function ( all_days ) {
      var google_map;

      google_map = {
        map: getMap(all_days),
        markers: getMarkers(all_days)
      };

      return google_map;
    };
  }
  

})();