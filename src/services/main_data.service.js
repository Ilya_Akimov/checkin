(function() {
    'use strict';
    
    angular
        .module('triblans')
        .service('MainDataService', MainDataService);

    function MainDataService() {

        var trip = {
            id: 1,
            title: "The Theasures of France",
            traveler_first_name: "Julia",
            traveler_second_name: "McBride",
            traveler_photo: "images/avatar.png",
            date: "Oct 24, 2016",
            check_ins: 67,
            photos: 92,
            description: "I'm Madeleine Richardson, travel writer by profession and lover of world languages, souls, food, oceans, wild spaces and urban places by nature. Since beginning...",
            days: [
                {
                    id: 1,
                    date: "13 April 2016",
                    time: "9:07 AM",
                    latitude: 35,
                    longitude: -86,
                    photos: [
                    ],
                    description: "45 years ago only farther up north were there any bridges spanning the river, which has its source in Spain and flows into the Atlantic some 1,007 km later. San Francisco’s Golden Gate Bridge in the US (despite its design being in fact based on the San Francisco – Oakland Bay Bridge in the same city).",
                    check_in: {
                    },
                    checkin_location: {
                        name: "Golden Gate Bridg",
                        city: "San Francisco",
                        country: "California"
                    },
                    likes: 5,
                    comments: [
                        {
                            id: 1,
                            time: "45 minutes ago",
                            commentator_first_name: "Annis",
                            commentator_second_name: "Horn",
                            commentator_photo: "images/avatar.png",
                            text: "Frederick was our guide for a day tour to Giverny and Auvers sur Oise"
                        },
                        {
                            id: 2,
                            time: "35 minutes ago",
                            commentator_first_name: "Annis",
                            commentator_second_name: "Horn",
                            commentator_photo: "images/avatar.png",
                            text: "Frederick was our guide for a day tour to Giverny and Auvers sur Oise"
                        }
                    ]
                },
                {
                    id: 2,
                    date: "13 April 2016",
                    time: "9:17 AM",
                    latitude: 32,
                    longitude: -87,
                    photos: [
                        { src: 'images/1.jpg', alt: 'image', title: 'image' },
                        { src: 'images/2.jpg', alt: 'image', title: 'image' },
                        { src: 'images/3.jpg', alt: 'image', title: 'image' },
                        { src: 'images/4.jpg', alt: 'image', title: 'image' },
                        { src: 'images/5.jpg', alt: 'image', title: 'image' },
                        { src: 'images/6.jpg', alt: 'image', title: 'image' },
                        { src: 'images/1.jpg', alt: 'image', title: 'image' },
                        { src: 'images/2.jpg', alt: 'image', title: 'image' },
                        { src: 'images/3.jpg', alt: 'image', title: 'image' },
                        { src: 'images/4.jpg', alt: 'image', title: 'image' }
                    ],
                    description: "45 years ago only farther up north were there any bridges spanning the river, which has its source in Spain and flows into the Atlantic some 1,007 km later. San Francisco’s Golden Gate Bridge in the US (despite its design being in fact based on the San Francisco – Oakland Bay Bridge in the same city).",
                    check_in: {
                    },
                    checkin_location: {
                        name: "Golden Gate Bridg",
                        city: "San Francisco",
                        country: "California"
                    },
                    likes: 4,
                    comments: [
                        {
                            id: 1,
                            time: "45 minutes ago",
                            commentator_first_name: "Annis",
                            commentator_second_name: "Horn",
                            commentator_photo: "images/avatar.png",
                            text: "Frederick was our guide for a day tour to Giverny and Auvers sur Oise"
                        },
                        {
                            id: 2,
                            time: "35 minutes ago",
                            commentator_first_name: "Annis",
                            commentator_second_name: "Horn",
                            commentator_photo: "images/avatar.png",
                            text: "Frederick was our guide for a day tour to Giverny and Auvers sur Oise"
                        },
                        {
                            id: 3,
                            time: "15 minutes ago",
                            commentator_first_name: "Annis",
                            commentator_second_name: "Horn",
                            commentator_photo: "images/avatar.png",
                            text: "Frederick was our guide for a day tour to Giverny and Auvers sur Oise"
                        }
                    ]
                },
                {
                    id: 3,
                    date: "18 April 2016",
                    time: "1:07 PM",
                    latitude: 35,
                    longitude: -87,
                    photos: [
                        { src: 'images/1.jpg', alt: 'image', title: 'image' },
                        { src: 'images/2.jpg', alt: 'image', title: 'image' },
                        { src: 'images/3.jpg', alt: 'image', title: 'image' },
                        { src: 'images/4.jpg', alt: 'image', title: 'image' },
                        { src: 'images/5.jpg', alt: 'image', title: 'image' },
                        { src: 'images/6.jpg', alt: 'image', title: 'image' },
                        { src: 'images/1.jpg', alt: 'image', title: 'image' },
                        { src: 'images/2.jpg', alt: 'image', title: 'image' },
                        { src: 'images/3.jpg', alt: 'image', title: 'image' },
                        { src: 'images/4.jpg', alt: 'image', title: 'image' }
                    ],
                    description: "45 years ago only farther up north were there any bridges spanning the river, which has its source in Spain and flows into the Atlantic some 1,007 km later. San Francisco’s Golden Gate Bridge in the US (despite its design being in fact based on the San Francisco – Oakland Bay Bridge in the same city).",
                    check_in: {
                        name: "Panorama Bar",
                        subtitle: "Review",
                        voted: 32,
                        rating: 5
                    },
                    checkin_location: {
                        name: "Golden Gate Bridg",
                        city: "San Francisco",
                        country: "California"
                    },
                    likes: 2,
                    comments: [
                        {
                            id: 1,
                            time: "45 minutes ago",
                            commentator_first_name: "Annis",
                            commentator_second_name: "Horn",
                            commentator_photo: "images/avatar.png",
                            text: "Frederick was our guide for a day tour to Giverny and Auvers sur Oise"
                        },
                        {
                            id: 2,
                            time: "35 minutes ago",
                            commentator_first_name: "Annis",
                            commentator_second_name: "Horn",
                            commentator_photo: "images/avatar.png",
                            text: "Frederick was our guide for a day tour to Giverny and Auvers sur Oise"
                        },
                        {
                            id: 3,
                            time: "15 minutes ago",
                            commentator_first_name: "Annis",
                            commentator_second_name: "Horn",
                            commentator_photo: "images/avatar.png",
                            text: "Frederick was our guide for a day tour to Giverny and Auvers sur Oise"
                        }
                    ]
                },
                {
                    id: 4,
                    date: "19 April 2016",
                    time: "1:07 PM",
                    latitude: 37,
                    longitude: -85,
                    photos: [
                        { src: 'images/1.jpg', alt: 'image', title: 'image' },
                        { src: 'images/2.jpg', alt: 'image', title: 'image' },
                        { src: 'images/3.jpg', alt: 'image', title: 'image' },
                        { src: 'images/4.jpg', alt: 'image', title: 'image' },
                        { src: 'images/5.jpg', alt: 'image', title: 'image' },
                        { src: 'images/6.jpg', alt: 'image', title: 'image' },
                        { src: 'images/1.jpg', alt: 'image', title: 'image' },
                        { src: 'images/2.jpg', alt: 'image', title: 'image' },
                        { src: 'images/3.jpg', alt: 'image', title: 'image' },
                        { src: 'images/4.jpg', alt: 'image', title: 'image' }
                    ],
                    description: "45 years ago only farther up north were there any bridges spanning the river, which has its source in Spain and flows into the Atlantic some 1,007 km later. San Francisco’s Golden Gate Bridge in the US (despite its design being in fact based on the San Francisco – Oakland Bay Bridge in the same city).",
                    check_in: {
                        name: "Panorama Bar",
                        subtitle: "Review",
                        voted: 32,
                        rating: 5
                    },
                    checkin_location: {
                        name: "Golden Gate Bridg",
                        city: "San Francisco",
                        country: "California"
                    },
                    likes: 2,
                    comments: [
                        {
                            id: 1,
                            time: "45 minutes ago",
                            commentator_first_name: "Annis",
                            commentator_second_name: "Horn",
                            commentator_photo: "images/avatar.png",
                            text: "Frederick was our guide for a day tour to Giverny and Auvers sur Oise"
                        },
                        {
                            id: 2,
                            time: "35 minutes ago",
                            commentator_first_name: "Annis",
                            commentator_second_name: "Horn",
                            commentator_photo: "images/avatar.png",
                            text: "Frederick was our guide for a day tour to Giverny and Auvers sur Oise"
                        },
                        {
                            id: 3,
                            time: "15 minutes ago",
                            commentator_first_name: "Annis",
                            commentator_second_name: "Horn",
                            commentator_photo: "images/avatar.png",
                            text: "Frederick was our guide for a day tour to Giverny and Auvers sur Oise"
                        }
                    ]
                }
            ]
        };

        this.getTrip = function() {
            return trip;
        };
    }

})();