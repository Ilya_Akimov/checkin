(function() {
  'use strict';

  angular
    .module('triblans')
    .service('MapService', MapService);

  MapService.$inject = [ 'uiGmapGoogleMapApi' ];
  function MapService ( uiGmapGoogleMapApi ) {

    // creating paths for images on map markers
    function getPaths ( all_days ) {
      var paths = [];

      angular.forEach(all_days, function(value) {
        var path = {};
        if (Object.keys(value.check_in).length != 0) {
          path.image = 'images/icon_marker_with_red_circle.png';
        } else if (value.photos.length!=0) {
          path.image = 'images/icon_photo_with_red_circle.png';
        } else {
          path.image = 'images/icon_text_with_red_circle.png';
        }
        path.latitude = value.latitude;
        path.longitude = value.longitude;
        paths.push(path);
      });

      return paths;
    }

    // Default settings for map marker
    function defaultMarker() {
      return {
        url: 'images/icon_red_circle.png',
        scaledSize: { width: 20, height: 20 },
        anchor: { x:10, y:10 },
        status: ''
      };
    }


    // create map with center values
    function getMap ( all_days ) {
        
      var map_center = {
        latitude: 0,
        longitude: 0
      };
      var map;
      
      angular.forEach(all_days, function(value) {
        map_center.latitude += value.latitude;
        map_center.longitude += value.longitude;
      });

      map_center.latitude = map_center.latitude / all_days.length;
      map_center.longitude = map_center.longitude / all_days.length;

      map = {
        center: map_center,
        zoom: 6,
        options: { 
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.TERRAIN
        }
      };
      return map;
    }

    // Creating of poplyline which is view a trip
    function getPolylines ( all_days ) {
      var paths = getPaths(all_days);
      var polylines;

      polylines = [
        {
          id: 1,
          path: paths,
          stroke: {
            color: '#f25b53',
            weight: 3
          },
          fill: {
            weight: 10
          },
          clickable: true,
          editable: false,
          draggable: false,
          geodesic: true,
          visible: true
        }
      ];
      return polylines;
    }

    function getMarkers ( all_days ) {

      var paths = getPaths(all_days);

      var markers = [];

      angular.forEach(paths, function(value, key) {
        var marker = {
          idKey: key+1,
          coords: {
            latitude: value.latitude,
            longitude: value.longitude
          },
          icon: defaultMarker(),
          image: value.image,
          events: {
            click: function(marker) {
          
              for (var i=0; i < markers.length; i++) {
                if (markers[i].idKey !== marker.key) {
                  markers[i].icon = defaultMarker();
                } else {
                  markers[i].icon = {
                    url: value.image,
                    scaledSize: { width: 30, height: 30 },
                    anchor: { x: 15, y: 15 },
                    status: 'active'
                  };
                }
              }

              $('html, body').animate({
                  scrollTop: $("#note" + marker.key).offset().top - 200
              }, 1000);

            },
            mouseover: function(marker) {
              if ( marker.icon.status !== 'active' ) {
                
                markers[marker.key-1].icon = {
                  url: value.image,
                  scaledSize: { width: 30, height: 30 },
                  anchor: { x: 15, y: 15 },
                  status: 'hover'
                };

              }
            },
            mouseout: function(marker) {
              if ( marker.icon.status !== 'active' ) {

                markers[marker.key-1].icon = defaultMarker();

              }
            }
          }
        };

        markers.push(marker);
      });

      return markers;
    }

    // main function which return completed map to controller
    this.createTripMap = function ( all_days ) {
      var google_map = {
        map: getMap(all_days),
        polylines: getPolylines(all_days),
        markers: getMarkers(all_days)
      };

      uiGmapGoogleMapApi.then(function(){
        google_map.polylines;
      });

      return google_map;
    };

    // for modal map
    this.getModalMap = function () {
      var map_center = {
        latitude: 0,
        longitude: 0
      };
      var map;

      map = {
        center: map_center,
        zoom: 6,
        bounds: {},
        options: {
          scrollwheel: false,
          mapTypeId: google.maps.MapTypeId.TERRAIN
        },
        control: {}
      };
      return map;
    };

  }
  

})();