(function () {
  'use strict';

  angular
    .module('triblans')
    .factory('utils', utils);

  utils.$inject = [];

  function utils() {
    return {
      debounce: debounce
    };

    ////////////////

    /**
     * Function decorator for call delay.
     * @param {Function} func - Source function
     * @param {Number} wait - ms
     * @param {Boolean} immediate - Flag switch debounce (true) and throttle (false) behavior
     * @returns {Function}
     */
    function debounce(func, wait, immediate) {
      var timeout;
      return function() {
        var context = this, args = arguments;
        var later = function() {
          timeout = null;
          if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    }
  }

}());