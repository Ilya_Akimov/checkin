(function() {
  'use strict';

  angular
    .module('triblans')
    .service('TripCheckinDataService', TripCheckinDataService);

  function TripCheckinDataService() {

    var short_trip = [
      {
        id: 1,
        country: 'Italy',
        city:
          [
            {
              id: 1,
              name:'Venice',
              center: {
                latitude:45.43697585375198,
                longitude:12.3410595
              }
            },
            {
              id: 2,
              name:'Rome',
              center: {
                latitude:41.8973973793116,
                longitude:12.49891149999997
              }
            },
            {
              id: 3,
              name: 'Florencia',
              center: {
                latitude:43.781908553528496,
                longitude:11.239657999999984
              }
            }
          ]
      },
      {
        id: 2,
        country: 'United Kingdom',
        city:
          [
            {
              id: 1,
              name: 'London',
              center: {
                latitude:51.48265070696638,
                longitude:-0.14209550000004564
              }
            }
          ]
      },
      {
        id: 3,
        country: 'Norway',
        city:
          [
            {
              id: 1,
              name: 'Geneva'
            }
          ]
      },
      {
        id: 4,
        country: 'Brazil',
        city:
          [
            {
              id: 1,
              name: 'Brasilia'
            }
          ]
      },
      {
        id: 5,
        country: 'Canada',
        city:
          [
            {
              id: 1,
              name: 'Ottawa'
            }
          ]
      },
      {
        id: 6,
        country: 'Cuba',
        city:
          [
            {
              id: 1,
              name: 'Havana'
            }
          ]
      },
      {
        id: 7,
        country: 'Morocco',
        city:
          [
            {
              id: 1,
              name: 'Rabat'
            },
            {
              id: 2,
              name: 'Fes'
            },
            {
              id: 3,
              name: 'Casabla'
            }
          ]
      },
      {
        id: 8,
        country: 'Finland',
        city:
          [
            {
              id: 1,
              name: 'Helsinki'
            }
          ]
      },
      {
        id: 9,
        country: 'Germany',
        city:
          [
            {
              id: 1,
              name: 'Berlin'
            }
          ]
      }
    ];

    var trip = {
      id: 1,
      title: "The Theasures of France",
      traveler_first_name: "Julia",
      traveler_second_name: "McBride",
      traveler_photo: "images/avatar.png",
      date: "Oct 24, 2016",
      check_ins: 67,
      photos: 92,
      country: [
        {
          id: 1,
          flag: "images/Italy.png",
          name: 'Italy',
          city: [
            {
              id:1,
              name: 'Venice',
              center: {
                latitude:45.43697585375198,
                longitude:12.3410595
              },
              content: [
                {
                  id: 1,
                  icon: "images/accommodation.png",
                  name: 'Accommodation',
                  latitude: 45.43697585375198,
                  longitude: 12.3410595,
                  note: [
                    {
                      id:1,
                      image: 'images/accommodation_photo.png',
                      form: 'form',
                      pay: '$599',
                      category:'Hotel and spa',
                      name: 'Champs Elysees Plaza',
                      site: 'Booking.com',
                      total_rating: 7,
                      total_status: 'Exeptional',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'Preserved defective offending he daughters on or. Rejoiced prospect yet material servants out answered men admitted. Sportsmen certainty prevailed suspected am as. Add stairs admire all answer the nearer yet length. Advantages prosperous remarkably my inhabiting so reasonably be if. Too any appearance announcing impossible one. Out mrs means heart ham tears shall power every.'
                      },
                      tags: ['Luxury', 'Family', 'Foodie']
                    },
                    {
                      id:2,
                      image: 'images/accommodation_photo.png',
                      form: 'form',
                      pay: '$499',
                      category:'Hotel and spa',
                      name: 'Champs Elysees Plaza',
                      site: 'Booking.com',
                      total_rating: 9.5,
                      total_status: 'Exeptional',
                      soufians_rating: 4,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'www www www www w www w ww w ww w www w ww w ww wwww www www ww  wwwww www www w wwwww w wwww ww wwww ww w wwww w ww w www w www www www w www www w ww w www w w w w w wwwwww  w  wwwww  w w   wwwwwww  w w w wwwwww'
                      },
                      tags: ['Luxury', 'Family', 'Foodie', 'History']
                    },
                    {
                      id:3,
                      image: 'images/accommodation_photo.png',
                      form: 'form',
                      pay: '$299',
                      category:'Hotel and spa',
                      name: 'Champs Elysees Plaza',
                      site: 'Booking.com',
                      total_rating: 7,
                      total_status: 'Exeptional',
                      soufians_rating: 4,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'iiiiiiiiii i i i iiiiiiiiiiiiiii i i i iiii i iiii iii iii iiiii iii iiiiiiiii iiii iiii ii ii iii iii ii iii i iii iii ii iii i ii i ii ii i ii i ii'
                      },
                      tags: ['Luxury', 'Family', 'Foodie']
                    }
                  ]
                },
                {
                  id: 2,
                  icon: 'images/to_do.png',
                  name: 'To do',
                  latitude: 45.47697585375198,
                  longitude: 12.3910595,
                  note: [
                    {
                      id:1,
                      image: 'images/louvre.png',
                      category:'Museum',
                      name: 'Louvre',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Local culture', 'History']
                    },
                    {
                      id:2,
                      image: 'images/louvre.png',
                      category:'Museum',
                      name: 'Louvre',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Local culture', 'History']
                    },
                    {
                      id:3,
                      image: 'images/louvre.png',
                      category:'Museum',
                      name: 'Louvre',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Local culture', 'History']
                    }
                  ]
                },
                {
                  id: 3,
                  icon: 'images/to_eat.png',
                  name: 'To eat',
                  latitude: 45.43997585375198,
                  longitude: 12.3490595,
                  note: [
                    {
                      id:1,
                      image: 'images/louvre.png',
                      category:'Cafe',
                      name: 'Bistro Volnay',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family', 'Foodie']

                    },
                    {
                      id:2,
                      image: 'images/louvre.png',
                      category:'Cafe',
                      name: 'Bistro Volnay',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family', 'Foodie']
                    },
                    {
                      id:3,
                      image: 'images/louvre.png',
                      category:'Cafe',
                      name: 'Bistro Volnay',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family', 'Foodie']
                    }
                  ]
                },
                {
                  id: 4,
                  icon: 'images/to_drink.png',
                  name: 'To drink',
                  latitude: 45.43697585375198,
                  longitude: 12.3410595,
                  note: [
                    {
                      id:1,
                      image: 'images/restaurant.png',
                      category:'Cafe',
                      name: 'Ten Belles',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Family', 'Foodie']

                    },
                    {
                      id:2,
                      image: 'images/restaurant.png',
                      category:'Cafe',
                      name: 'Ten Belles',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Family', 'Foodie']
                    },
                    {
                      id:3,
                      image: 'images/restaurant.png',
                      category:'Cafe',
                      name: 'Ten Belles',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Family', 'Foodie']
                    }
                  ]
                },
                {
                  id: 5,
                  icon: 'images/to_shop.png',
                  name: 'To shop',
                  latitude: 45.48697585375198,
                  longitude: 12.3910595,
                  note: [
                    {
                      id:1,
                      image: 'images/Boutique.png',
                      category:'Boutique',
                      name: 'Nordik Market',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family']

                    },
                    {
                      id:2,
                      image: 'images/Boutique.png',
                      category:'Boutique',
                      name: 'Nordik Market',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family']
                    },
                    {
                      id:3,
                      image: 'images/Boutique.png',
                      category:'Boutique',
                      name: 'Nordik Market',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family']
                    }
                  ]
                }
              ]
            },
            {
              id:2,
              name: 'Rome',
              center: {
                latitude:41.8973973793116,
                longitude:12.49891149999997
              },
              content: [
                {
                  id: 1,
                  icon: "images/accommodation.png",
                  name: 'Accommodation',
                  latitude: 41.89632326087528,
                  longitude: 12.497881531738265,
                  note: [
                    {
                      id:1,
                      image: 'images/accommodation_photo.png',
                      form: 'form',
                      pay: '$599',
                      category:'Hotel and spa',
                      name: 'Champs Elysees Plaza',
                      site: 'Booking.com',
                      total_rating: 7,
                      total_status: 'Exeptional',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      tags: ['Luxury', 'Family', 'Foodie']
                    },
                    {
                      id:2,
                      image: 'images/accommodation_photo.png',
                      form: 'form',
                      pay: '$499',
                      category:'Hotel and spa',
                      name: 'Champs Elysees Plaza',
                      site: 'Booking.com',
                      total_rating: 9.5,
                      total_status: 'Exeptional',
                      soufians_rating: 4,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      tags: ['Luxury', 'Family', 'Foodie']
                    },
                    {
                      id:3,
                      image: 'images/accommodation_photo.png',
                      form: 'form',
                      pay: '$299',
                      category:'Hotel and spa',
                      name: 'Champs Elysees Plaza',
                      site: 'Booking.com',
                      total_rating: 7,
                      total_status: 'Exeptional',
                      soufians_rating: 4,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      tags: ['Luxury', 'Family', 'Foodie']
                    }
                  ]
                },
                {
                  id: 2,
                  icon: 'images/to_do.png',
                  name: 'To do',
                  latitude: 41.89732326087528,
                  longitude: 12.494881531738265,
                  note: [
                    {
                      id:1,
                      image: 'images/louvre.png',
                      category:'Museum',
                      name: 'Louvre',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Local culture', 'History']
                    },
                    {
                      id:2,
                      image: 'images/louvre.png',
                      category:'Museum',
                      name: 'Louvre',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Local culture', 'History']
                    },
                    {
                      id:3,
                      image: 'images/louvre.png',
                      category:'Museum',
                      name: 'Louvre',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Local culture', 'History']
                    }
                  ]
                },
                {
                  id: 3,
                  icon: 'images/to_eat.png',
                  name: 'To eat',
                  latitude: 41.86632326087528,
                  longitude: 12.477881531738265,
                  note: [
                    {
                      id:1,
                      image: 'images/louvre.png',
                      category:'Cafe',
                      name: 'Bistro Volnay',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family', 'Foodie']

                    },
                    {
                      id:2,
                      image: 'images/louvre.png',
                      category:'Cafe',
                      name: 'Bistro Volnay',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family', 'Foodie']
                    },
                    {
                      id:3,
                      image: 'images/louvre.png',
                      category:'Cafe',
                      name: 'Bistro Volnay',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family', 'Foodie']
                    }
                  ]
                },
                {
                  id: 4,
                  icon: 'images/to_drink.png',
                  name: 'To drink',
                  latitude: 41.89432326087528,
                  longitude: 12.494881531738265,
                  note: [
                    {
                      id:1,
                      image: 'images/restaurant.png',
                      category:'Cafe',
                      name: 'Ten Belles',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Family', 'Foodie']

                    },
                    {
                      id:2,
                      image: 'images/restaurant.png',
                      category:'Cafe',
                      name: 'Ten Belles',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Family', 'Foodie']
                    },
                    {
                      id:3,
                      image: 'images/restaurant.png',
                      category:'Cafe',
                      name: 'Ten Belles',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Family', 'Foodie']
                    }
                  ]
                },
                {
                  id: 5,
                  icon: 'images/to_shop.png',
                  name: 'To shop',
                  latitude: 41.85632326087528,
                  longitude: 12.477881531738265,
                  note: [
                    {
                      id:1,
                      image: 'images/Boutique.png',
                      category:'Boutique',
                      name: 'Nordik Market',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family']

                    },
                    {
                      id:2,
                      image: 'images/Boutique.png',
                      category:'Boutique',
                      name: 'Nordik Market',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family']
                    },
                    {
                      id:3,
                      image: 'images/Boutique.png',
                      category:'Boutique',
                      name: 'Nordik Market',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family']
                    }
                  ]
                }
              ]
            },
            {
              id:3,
              name: 'Florencia',
              center: {
                latitude:43.781908553528496,
                longitude:11.239657999999984
              },
              content: [
                {
                  id: 1,
                  icon: "images/accommodation.png",
                  name: 'Accommodation',
                  latitude: 43.781908553528496,
                  longitude: 11.29657999999984,
                  note: [
                    {
                      id:1,
                      image: 'images/accommodation_photo.png',
                      form: 'form',
                      pay: '$599',
                      category:'Hotel and spa',
                      name: 'Champs Elysees Plaza',
                      site: 'Booking.com',
                      total_rating: 7,
                      total_status: 'Exeptional',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      tags: ['Luxury', 'Family', 'Foodie']
                    },
                    {
                      id:2,
                      image: 'images/accommodation_photo.png',
                      form: 'form',
                      pay: '$499',
                      category:'Hotel and spa',
                      name: 'Champs Elysees Plaza',
                      site: 'Booking.com',
                      total_rating: 9.5,
                      total_status: 'Exeptional',
                      soufians_rating: 4,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      tags: ['Luxury', 'Family', 'Foodie']
                    },
                    {
                      id:3,
                      image: 'images/accommodation_photo.png',
                      form: 'form',
                      pay: '$299',
                      category:'Hotel and spa',
                      name: 'Champs Elysees Plaza',
                      site: 'Booking.com',
                      total_rating: 7,
                      total_status: 'Exeptional',
                      soufians_rating: 4,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      tags: ['Luxury', 'Family', 'Foodie']
                    }
                  ]
                },
                {
                  id: 2,
                  icon: 'images/to_do.png',
                  name: 'To do',
                  latitude: 43.771908553528496,
                  longitude: 11.25657999999984,
                  note: [
                    {
                      id:1,
                      image: 'images/louvre.png',
                      category:'Museum',
                      name: 'Louvre',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Local culture', 'History']
                    },
                    {
                      id:2,
                      image: 'images/louvre.png',
                      category:'Museum',
                      name: 'Louvre',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Local culture', 'History']
                    },
                    {
                      id:3,
                      image: 'images/louvre.png',
                      category:'Museum',
                      name: 'Louvre',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Local culture', 'History']
                    }
                  ]
                },
                {
                  id: 3,
                  icon: 'images/to_eat.png',
                  name: 'To eat',
                  latitude: 43.721908553528496,
                  longitude: 11.29457999999984,
                  note: [
                    {
                      id:1,
                      image: 'images/louvre.png',
                      category:'Cafe',
                      name: 'Bistro Volnay',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family', 'Foodie']

                    },
                    {
                      id:2,
                      image: 'images/louvre.png',
                      category:'Cafe',
                      name: 'Bistro Volnay',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family', 'Foodie']
                    },
                    {
                      id:3,
                      image: 'images/louvre.png',
                      category:'Cafe',
                      name: 'Bistro Volnay',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family', 'Foodie']
                    }
                  ]
                },
                {
                  id: 4,
                  icon: 'images/to_drink.png',
                  name: 'To drink',
                  latitude: 43.751908553528496,
                  longitude: 11.23657999999984,
                  note: [
                    {
                      id:1,
                      image: 'images/restaurant.png',
                      category:'Cafe',
                      name: 'Ten Belles',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Family', 'Foodie']

                    },
                    {
                      id:2,
                      image: 'images/restaurant.png',
                      category:'Cafe',
                      name: 'Ten Belles',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Family', 'Foodie']
                    },
                    {
                      id:3,
                      image: 'images/restaurant.png',
                      category:'Cafe',
                      name: 'Ten Belles',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Family', 'Foodie']
                    }
                  ]
                },
                {
                  id: 5,
                  icon: 'images/to_shop.png',
                  name: 'To shop',
                  latitude: 43.789908553528496,
                  longitude: 11.25657999999984,
                  note: [
                    {
                      id:1,
                      image: 'images/Boutique.png',
                      category:'Boutique',
                      name: 'Nordik Market',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family']

                    },
                    {
                      id:2,
                      image: 'images/Boutique.png',
                      category:'Boutique',
                      name: 'Nordik Market',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family']
                    },
                    {
                      id:3,
                      image: 'images/Boutique.png',
                      category:'Boutique',
                      name: 'Nordik Market',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family']
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          id: 2,
          flag: "images/Italy.png",
          name: 'United Kingdom',
          city: [
            {
              id:1,
              name: 'London',
              center: {
                latitude:51.48265070696638,
                longitude:-0.14209550000004564
              },
              content: [
                {
                  id: 1,
                  icon: "images/accommodation.png",
                  name: 'Accommodation',
                  latitude: 51.4762193427945,
                  longitude: -0.08750718212895191,
                  note: [
                    {
                      id:1,
                      image: 'images/accommodation_photo.png',
                      form: 'form',
                      pay: '$599',
                      category:'Hotel and spa',
                      name: 'Champs Elysees Plaza',
                      site: 'Booking.com',
                      total_rating: 7,
                      total_status: 'Exeptional',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      tags: ['Luxury', 'Family', 'Foodie']
                    },
                    {
                      id:2,
                      image: 'images/accommodation_photo.png',
                      form: 'form',
                      pay: '$499',
                      category:'Hotel and spa',
                      name: 'Champs Elysees Plaza',
                      site: 'Booking.com',
                      total_rating: 9.5,
                      total_status: 'Exeptional',
                      soufians_rating: 4,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      tags: ['Luxury', 'Family', 'Foodie']
                    },
                    {
                      id:3,
                      image: 'images/accommodation_photo.png',
                      form: 'form',
                      pay: '$299',
                      category:'Hotel and spa',
                      name: 'Champs Elysees Plaza',
                      site: 'Booking.com',
                      total_rating: 7,
                      total_status: 'Exeptional',
                      soufians_rating: 4,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      tags: ['Luxury', 'Family', 'Foodie']
                    }
                  ]
                },
                {
                  id: 2,
                  icon: 'images/to_do.png',
                  name: 'To do',
                  latitude: 51.4462193427945,
                  longitude: -0.06750718212895191,
                  note: [
                    {
                      id:1,
                      image: 'images/louvre.png',
                      category:'Museum',
                      name: 'Louvre',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Local culture', 'History']
                    },
                    {
                      id:2,
                      image: 'images/louvre.png',
                      category:'Museum',
                      name: 'Louvre',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Local culture', 'History']
                    },
                    {
                      id:3,
                      image: 'images/louvre.png',
                      category:'Museum',
                      name: 'Louvre',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Local culture', 'History']
                    }
                  ]
                },
                {
                  id: 3,
                  icon: 'images/to_eat.png',
                  name: 'To eat',
                  latitude: 51.4792193427945,
                  longitude: -0.08350718212895191,
                  note: [
                    {
                      id:1,
                      image: 'images/louvre.png',
                      category:'Cafe',
                      name: 'Bistro Volnay',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family', 'Foodie']

                    },
                    {
                      id:2,
                      image: 'images/louvre.png',
                      category:'Cafe',
                      name: 'Bistro Volnay',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family', 'Foodie']
                    },
                    {
                      id:3,
                      image: 'images/louvre.png',
                      category:'Cafe',
                      name: 'Bistro Volnay',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family', 'Foodie']
                    }
                  ]
                },
                {
                  id: 4,
                  icon: 'images/to_drink.png',
                  name: 'To drink',
                  latitude: 51.4722193427945,
                  longitude: -0.08150718212895191,
                  note: [
                    {
                      id:1,
                      image: 'images/restaurant.png',
                      category:'Cafe',
                      name: 'Ten Belles',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Family', 'Foodie']

                    },
                    {
                      id:2,
                      image: 'images/restaurant.png',
                      category:'Cafe',
                      name: 'Ten Belles',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Family', 'Foodie']
                    },
                    {
                      id:3,
                      image: 'images/restaurant.png',
                      category:'Cafe',
                      name: 'Ten Belles',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Family', 'Foodie']
                    }
                  ]
                },
                {
                  id: 5,
                  icon: 'images/to_shop.png',
                  name: 'To shop',
                  latitude: 51.4732193427945,
                  longitude: -0.08350718212895191,
                  note: [
                    {
                      id:1,
                      image: 'images/Boutique.png',
                      category:'Boutique',
                      name: 'Nordik Market',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family']

                    },
                    {
                      id:2,
                      image: 'images/Boutique.png',
                      category:'Boutique',
                      name: 'Nordik Market',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family']
                    },
                    {
                      id:3,
                      image: 'images/Boutique.png',
                      category:'Boutique',
                      name: 'Nordik Market',
                      soufians_rating: 5,
                      comment: {
                        title: 'Amazing stay... Amazing view',
                        content: 'You have all options of transportation close to you. The staff is very nice and most peaple speak that good place'
                      },
                      location: 'Avenue Paris',
                      tags: ['Luxury', 'Family']
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    };

    this.getTrip = function() {
      return trip;
    };
    this.getShortTrip =function() {
      return short_trip;
    }
  }

})();