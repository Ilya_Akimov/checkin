(function() {
    'use strict';

    var dep = [
        'uiGmapgoogle-maps',
        'ngAutocomplete',
        'ui.bootstrap',
        'ui.timepicker'
    ];

    config.$inject = [
        'uiGmapGoogleMapApiProvider'
    ];

    function config (GoogleMapApiProviders) {
        GoogleMapApiProviders.configure({
            china: true
        });
    }

    angular
        .module('triblans', dep)
        .config(config);
        
})();